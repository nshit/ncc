package 
{
	import css.textChar;
	import css.textTop;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.net.SharedObject;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	import ncc.Account;
	import ncc.Character;
	import ncc.msnAnalisa;
	import ncc.pAmf;
	import ncc.ZendAMFClient;
	import ncc.Panel;
	import ncc.charPanel;
	import ncc.mcMsn;
	import ncc.mcExam;
	import ncc.mcAnalisa;
	import ncc.jouninClass;
	import ncc.mcAmf;
	import ncc.base;
	import flash.events.ErrorEvent;
	import com.adobe.crypto.SHA1;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkErrorEvent;
	import flash.display.LoaderInfo;
	/**
	 * @mxmlc -o build/ninja_saga.swf -library-path=swc
	 * @author masder, support by knights of destiny, official sites http://ninjasaga.cheat.center/, version 2.1.1184
	 */
	[SWF(width="960", height="780", frameRate="24", backgroundColor="#000000")]
	public class Ninja_saga extends Sprite {
		public var amf:ZendAMFClient;
		public var acc:Account;
		public var info:textTop;
		public var panel:Panel;
		public var mf:mcAmf;
		public var charp:charPanel;
		public var mmsn:mcMsn;
		public var mexam:mcExam;
		public var mcatm:mcAnalisa;
		public var curmc:*;
		public var tmp:SharedObject;
		public var ses:String = "";
		public var ldr:BulkLoader;
		public var mph:String="http://cdn.static.ninjasaga.com/swf/";
		public var char:Character;
		public var sertime:uint = 0;
		public var curdate:uint = 0;
		public var difdate:uint = 0;
		public var tm:Timer;
		public var datamsn:Object;
		public var bpmsn:Array = [];
		public var onair:Boolean = false;
		public static const listexam:Object = { chunin:[55, 56, 57, 58, 59], jounin:[132, 133, 134, 135, 136], sjounin:[226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238], tsjounin:[200, 205, 202, 206, 203, 207, 204, 208, 201, 209, 210, 211, 212], tutor:[250, 252, 249, 253, 248, 254, 247, 255, 251, 256, 257, 258], senin:[266, 259, 267, 260, 268, 261, 270, 262, 269, 263, 264, 265] };
		public var keyexam:String = "";
		private var listchar:Array = [];
		private var seq:uint = 0;
		private var pf:String = "facebook";
		private var _s:String = "Vmn34aAciYK00Hen26nT01";
		private var msnPanel:Array=[];
		private var msnPanelHash:String="";
		private var seninPanel:Array=[];
		private var seninPanelHash:String="";
		private var poscharlist:Array = [ { x:164, y:50 }, { x:504, y:50 }, { x:164, y:170 }, { x:504, y:170 }, { x:164, y:290 }, { x:504, y:290 } ];
		private var listmsn:Vector.<pAmf>;
		private var curmsn:pAmf;
		private var fn:Function;
		private var espos:int = 0;
		private var estpos:int = 0;
		private var estm:int = 0;
		private var esttm:int = 0;
		private var esctm:int = 0;
		private var lastmsntime:uint = 0;
		private var ldrfn:Function;
		private var ldrpath:Object = { };
		private var disexam:Boolean;
		private var tutorShow:Boolean = false;
		private var amfExam:pAmf;
		private var counter:Timer;
		private var mclass:Sprite;
		private var analis:msnAnalisa;
		
		public function Ninja_saga():void {
			var a:int = 999;
			base.main = this;	
			graphics.lineStyle(1, 0xd8dfea);
			graphics.beginFill(0xedeff4);
			graphics.drawRect(0, 0, stage.stageWidth - 1, stage.stageHeight);
			graphics.endFill();
			acc = new Account;
			acc.uid = stage.loaderInfo.parameters.fb_uid;
			info = new textTop(this);
			tmp = SharedObject.getLocal("Ninja_saga_amf");
			if (tmp.data.amf == null) {
				tmp.data.amf = 1;
				setTmp(0);
			}else {
				if (tmp.data.status == 1) {
					tmp.data.amf++;
					if (tmp.data.amf > 2) tmp.data.amf = 1;
					tmp.flush();
				}
			}
			amf = ZendAMFClient.getInstance();
			amf.addEventListener(ErrorEvent.ERROR, onFault);
			if (!amf.isConnected()) amf.connect("http://app.ninjasaga.com/amf_live" + String(tmp.data.amf)+"/");
			send("SystemService.requireLogin", [stage.loaderInfo.parameters.time, stage.loaderInfo.parameters.hash_time, acc.uid], requireLoginRes);
		}
		private function requireLoginRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			var b:String = a.result + acc.uid + pf + stage.loaderInfo.parameters.client_version + "85224034668";
			var c:String = getLoginHash(a.result, b);
			send("SystemService.snsLogin",[acc.uid,pf,stage.loaderInfo.parameters.client_version,a.result,c,stage.loaderInfo.parameters.fb_sig,stage.loaderInfo.parameters.fb_at,"en"],snsLoginRes);
		}
		public function setTmp(a:int):void {
			tmp.data.status = a;
			tmp.flush();
		}
		private function snsLoginRes(a:Object):void {
			if (a.status == 0) {
				if (a.error == 102) {
					setTmp(1);
					return error("amf_live"+tmp.data.amf+" Error, reload...",true);
				}
				return error(String(a.error));
			}
			setTmp(0);
			acc.id = a.result[0];
			acc.prem = a.result[1];
			acc.token = a.result[2];
			ses = a.result[3];
			ldr = new BulkLoader("ninja_cheat_center");
			mph=stage.loaderInfo.url.split("ninja_saga.swf")[0]+"swf/";
			loadSwf({lib:"language/data_library_en.swf",msn:"panels/mission_2.swf"},dataLibFinish);
		}
		private function addLdr(a:String,b:String):void {
			ldrpath[a]=b;
			ldr.add(b);
		}
		private function loadFinish(a:Event):void {
			ldr.removeEventListener(Event.COMPLETE,loadFinish);
			ldr.removeEventListener(BulkErrorEvent.ERROR, loaderError);
			info.text = "loadSwf... Finish";
			if (ldrfn != null) ldrfn();
		}
		public function loadSwf(a:Object, b:Function):void {
			ldrfn = b;
			for(var c:String in a) {
				addLdr(c, mph+a[c]);
			}
			ldr.addEventListener(Event.COMPLETE,loadFinish);
			ldr.addEventListener(BulkErrorEvent.ERROR, loaderError);
			info.text = "loadSwf...";
			ldr.start();
		}
		private function loaderError(a:BulkErrorEvent):void {
			ldr.removeEventListener(Event.COMPLETE,dataLibFinish);
			ldr.removeEventListener(BulkErrorEvent.ERROR,loaderError);
			ldr=null;
			error("file not found.");
		}
		private function dataLibFinish():void {
			datamsn = MovieClip(ldr.getMovieClip(ldrpath.lib)).getMissionDetail();
			var b:LoaderInfo = MovieClip(ldr.getMovieClip(ldrpath.msn)).loaderInfo;
			if (datamsn == null) {
				error("MissionDetail not found");
				return;
			}
			msnPanel=[[b.url, b.bytesTotal, b.bytesLoaded, true, b.swfVersion, b.actionScriptVersion, b.parameters]];
			msnPanelHash = getHash(msnPanel[1]);
			send("CharacterDAO.getCharactersList", [ses], getCharactersListRes);
		}
		private function getCharactersListRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			if (a.result.length == 0) return error("You don't have character, Please create at least one character.");
			info.text = "Character List";
			var b:textChar;
			for (var i:int = 0, j:int = a.result.length>6?6:a.result.length; i < j; i++) {
				b = new textChar(this, a.result[i], poscharlist[i], getCharacterById);
				listchar.push(b);
			}
		}
		public function getCharacterById(a:MouseEvent):void {
			var b:int = (a.currentTarget as textChar).charid;
			for (var i:int = 0, j:int = listchar.length; i < j; i++) {
				removeChild(listchar[i] as textChar);
			}
			send("CharacterDAO.getCharacterById", [ses, b], getCharacterByIdRes);
		}
		private function getCharacterByIdRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			char = new Character(a.result);
			if (ObjectLength(char.msn) == 0) {
				error("Mission Empty, Please do mission manual from level 1 to 3");
				return;
			}
			send("SystemData.get", [ses, false], getSystemDataRes);
		}
		private function getSystemDataRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			if (a.result.server_time) sertime = int(a.result.server_time);
			if (a.result.current_date) curdate = int(a.result.current_date);
			if (sertime > 0) {
				var b:Date = new Date();
				difdate = Math.round(b.getTime() / 1000) - sertime;
			}
			send("CharacterDAO.getExtraData", [ses,getHash(String(char.xp)),stage.loaderInfo.parameters.fb_at], getExtraDataRes);
		}
		private function getExtraDataRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			if (a.result.bp_mission_id) bpmsn = a.result.bp_mission_id as Array;
			if (a.result.sje_end_date) char.startSJ = int(a.result.sje_end_date);
			if (a.result.se_day_count_open) char.startTutor = int(a.result.se_day_count_open);
			if (a.result.se_end_date_notice) char.noticeTutor = int(a.result.se_end_date_notice);
			if (a.result.se_end_date) char.tutorLeft = int(a.result.se_end_date);
			if (a.result.player_pet) char.parsePet(a.result.player_pet as Array);
			info.text = "http://ninjasaga.cheat.center/";
			listmsn = new Vector.<pAmf>();
			counter = new Timer(1000);
			counter.addEventListener(TimerEvent.TIMER, fnCounter);
			charp = new charPanel(this, [char.id, char.name, char.level, char.xp, char.rank, char.gold, char.gender, acc.token, acc.prem, char.tp, char.pet.name, char.pet.id, char.pet.level]);
			if (char.level >= 60 && char.rank < 5 && char.control <= 0) {
				isexam();
			}
			analis = new msnAnalisa();
			mmsn = new mcMsn(this);
			mcatm = new mcAnalisa(this,doAtm,calcMsn);
			mexam = new mcExam(this);
			mf = new mcAmf(this);
			panel = new Panel(this, [clickBtnMsn, clickBtnLevel, clickBtnXP, clickBtnGold, clickBtnExam]);
		}
		private function calcMsn(a:uint, b:uint):String {
			var c:uint;
			var d:uint;
			switch(a) {
				case 0:
					if (char.level >= b) {
						mcatm.amount.text = String(char.level + 1);
						return "";
					}
					if (b > 20 && char.rank < 2) {
						mcatm.amount.text = "20";
						return "";
					}
					if (b > 40 && char.rank < 4) {
						mcatm.amount.text = "40";
						return "";
					}
					if (b > 60 && char.rank < 6) {
						mcatm.amount.text = "60";
						return "";
					}
					if (b > 80) return "";
					return analis.xp(char.xp, Character.getXpByLv(b));
				case 1:
					if (b < 10) {
						mcatm.amount.text = "10";
						return "";
					}
					c = char.xp + b;
					d = Character.getLvByXp(c);
					if (d >= 20 && char.rank < 2) {
						c = Character.getXpByLv(20) - char.xp;
						if (c < 0) c = 0;
						mcatm.amount.text = String(c);
						return "";
					}
					if (d >= 40 && char.rank < 4) {
						c = Character.getXpByLv(40) - char.xp;
						if (c < 0) c = 0;
						mcatm.amount.text = String(c);
						return "";
					}
					if (d >= 60 && char.rank < 6) {
						c = Character.getXpByLv(60) - char.xp;
						if (c < 0) c = 0;
						mcatm.amount.text = String(c);
						return "";
					}
					if (d >= 80){
						c = Character.getXpByLv(80) - char.xp;
						if (c < 0) c = 0;
						mcatm.amount.text = String(c);
						return "";
					}	
					return analis.xp(char.xp, c);
				case 2:
					if (char.rank < 2) c = 20;
					else if (char.rank < 4) c = 40;
					else if (char.rank < 6) c = 60;
					else c = 80;
					return analis.gold(char.xp, Character.getXpByLv(c), b);
			}
			return "";
		}
		private function setJouninClass(a:uint):void {
			char.control = a;
			send("CharacterDAO.SJClassSelect", [ses, char.control], SJClassSelectRes);
		}
		private function SJClassSelectRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			if (mclass != null) {
				removeChild(mclass);
				mclass = null;
			}
			onair = false;
		}
		public function clickBtnMsn(a:MouseEvent):void {
			if (onair) return;
			if (curmc != null) curmc.visible = false;
			info.text = "Mission";
			curmc = mmsn;
			mmsn.msnShow();
		}
		public function clickBtnLevel(a:MouseEvent):void {
			if (onair) return;
			if (curmc != null) curmc.visible = false;
			info.text = "Level";
			curmc = mcatm;
			mcatm.mode = 0;
			mcatm.amount.text = String(char.level + 1);
		}
		public function clickBtnXP(a:MouseEvent):void {
			if (onair) return;
			if (curmc != null) curmc.visible = false;
			info.text = "XP";
			curmc = mcatm;
			mcatm.mode = 1;
			mcatm.amount.text = "10";
		}
		public function clickBtnGold(a:MouseEvent):void {
			if (onair) return;
			if (curmc != null) curmc.visible = false;
			info.text = "Gold";
			curmc = mcatm;
			mcatm.mode = 2;
			mcatm.amount.text = "1000000";
		}
		public function clickBtnExam(a:MouseEvent):void {
			if (onair) return;
			isexam();
			if (disexam) {
				if (curmc != null) curmc.visible = false;
				curmc = mexam;
				mexam.fn = doExam;
				info.text = "Exam";
				mexam.visible = true;
			}else {
				mexam.fn = null;
			}
		}
		private function isexam():void {
			var a:String, b:Array;
			disexam = false;
			keyexam = "";
			if (char.level == 20 && char.rank < 2) a = "chunin";
			else if (char.level == 40 && char.rank < 4) a = "jounin";
			else if (char.level == 60 && char.rank < 6) {
				if (char.startSJ < 0) {
					disexam = false;
					send("CharacterService.startSJExam", [ses], startSJExamRes);
					return;
				}else {
					if (char.startSJ > 0)a = "tsjounin";
					else a = "sjounin";
				}
			}else if (char.level == 80 && char.rank < 8) {
				if (char.noticeTutor==1&&char.noticeTutor==0&&char.tutorLeft==-1) {
					disexam = false;
					send("CharacterService.startNTExam", [ses], startNTExamRes);
					return;
				}else if (char.startTutor == 1 && char.noticeTutor == 1 && char.tutorLeft > -1&&char.tutorConfirm==true&&tutorShow==false) {
					disexam = false;
					send("CharacterDAO.NTExamNotice", [ses], NTExamNoticeRes);
					return;
				}else {
					tutorShow = false;
					if (char.tutorHard) a = "senin";
					else a = "tutor";
				}
			}else {
				return;
			}
			for (var c:int = 0, d:int = listexam[a].length; c < d;c++) {
				b = char.getMsn("msn" + String(listexam[a][c]));
				if (b[0] == 0) {
					mexam.len = listexam[a].length;
					mexam.pos = c;
					mexam.view(a);
					keyexam = a;
					var e:uint = keyexam == "senin" || keyexam == "tutor"?420:10;
					mexam.interval.text = String(e);
					amfExam = new pAmf("msn" + String(listexam[a][c]), char.level,0,0,0,1,e);
					disexam = true;
					return;
				}
			}
			switch(a) {
				case "chunin":
					send("CharacterDAO.rankup", [ses], onRankChunin);
					break;
				case "jounin":
					setrank(4);
					break;
				case "sjounin":
					setrank(6);
					choiceJouninClass();
					break;
				case "tsjounin":
					setrank(7);
					choiceJouninClass();
					break;
				case "tutor":
					setrank(8);
					break;
				case "senin":
					setrank(9);
					break;
			}
		}
		private function choiceJouninClass():void {
			if (curmc != null) {
				curmc.visible = false;
				curmc = null;
			}
			onair = true;
			mclass = new jouninClass(this, setJouninClass);
		}
		private function onRankChunin(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			setrank(2);
		}
		private function setrank(a:uint):void {
			char.rank = a;
			charp.setbadge(char.rank);
		}
		private function startSJExamRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			if (a.sje_end_date) {
				char.startSJ = a.sje_end_date;
				clickBtnExam(null);
			}
		}
		private function startNTExamRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			tutorShow = true;
			char.tutorHard = true;
			clickBtnExam(null);
		}
		private function NTExamNoticeRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			if(a.exam_status=="0"){
				if (a.time_left > 0) {
						tutorShow = true;
						char.tutorHard = true;
				}else {
					char.tutorConfirm = false;
					char.tutorHard = false;
				}
				clickBtnExam(null);
			}else{
				send("CharacterDAO.NTClassSelect", [ses], NTClassSelectRes);
			}	
		}
		private function NTClassSelectRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			
			clickBtnExam(null);
		}
		private function doExam(a:MouseEvent):void {
			mexam.visible = false;
			curmc = null;
			if (amfExam != null) {
				amfExam.interval = int(mexam.interval.text);
				if (amfExam.interval < 10) amfExam.interval = 10;
				preMsn(clickBtnExam,amfExam);
			}
		}
		public function doMsn(a:String, b:uint, c:uint, d:uint, e:uint, f:uint, g:uint):void {
			preMsn(clickBtnMsn, new pAmf(a, b, c, d, e, f, g));
		}
		private function doAtm(a:uint):void {
			mcatm.visible = false;
			curmc = null;
			if (analis != null) {
				if (analis.data.length > 0) {
					onair = true;
					curmsn = null;
					listmsn.length = 0;
					for each(var b:pAmf in analis.data.data){
						listmsn.push(b);
					}
					base.msnana = null;
					setEstimate();
					switch(a) {
						case 0:
							fn = clickBtnLevel;
							break;
						case 1:
							fn = clickBtnXP;
							break;
						case 2:
							fn = clickBtnGold;
							break;
						default:
							fn = clickBtnMsn;
						break;
					}
					mf.setInfo();
					mf.setStatus();
					mf.setbar();
					mf.onClickStop = doMsnFinish;
					mf.visible = true;
					tm = new Timer(10000);
					tm.addEventListener(TimerEvent.TIMER, timeFinish);
					qmsn();
				}
			}
		}
		private function preMsn(a:Function, b:pAmf):void {
			curmc = null;
			onair = true;
			curmsn = null;
			listmsn.length = 0;
			listmsn.push(b);
			setEstimate();
			fn = a;
			mf.setInfo();
			mf.setStatus();
			mf.setbar();
			mf.onClickStop = doMsnFinish;
			mf.visible = true;
			tm = new Timer(10000);
			tm.addEventListener(TimerEvent.TIMER, timeFinish);
			qmsn();
		}
		private function doMsnFinish(a:MouseEvent):void {
			counter.stop();
			tm.stop();
			tm.removeEventListener(TimerEvent.TIMER, timeFinish);
			curmsn = null;
			listmsn.length = 0;
			mf.visible = false;
			mf.setInfo();
			mf.setStatus();
			mf.setbar();
			mf.onClickStop = null;
			onair = false;
			if (fn != null) {
				fn(null);
				fn = null;
			}
		}
		private function fnCounter(a:TimerEvent):void {
			if (onair) {
				esctm++;
				mf.setbar(Math.round(esctm * 100 / esttm));
				mf.setInfo("Process " + String(espos) + " of " + String(estpos) + ", Estimated time remaining " + formatTime(esttm - esctm));
				mf.setStatus(String(curmsn.dbid)+". "+datamsn[curmsn.id].name+", Lv."+String(curmsn.level)+", XP: "+String(curmsn.xp)+", Gold: "+String(curmsn.gold));
			}else counter.stop();
		}
		private function qmsn():void {
			if (curmsn != null) {
				if (curmsn.repeat > 0) {
					curmsn.repeat--;
					tm.delay = curmsn.interval * 1000;
					esctm = estm;
					espos++;
					mf.setInfo("Process " + String(espos) + " of " + String(estpos) + ", Estimated time remaining " + formatTime(esttm - esctm));
					mf.setStatus(String(curmsn.dbid)+". "+datamsn[curmsn.id].name+", Lv."+String(curmsn.level)+", XP: "+String(curmsn.xp)+", Gold: "+String(curmsn.gold));
					estm += curmsn.interval;
					send("FileChecking.checkHackActivity", [ses, msnPanel, msnPanelHash], onCheckingHackRes);
					return;
				}
			}
			if(listmsn.length>0){
				curmsn = listmsn.shift();
				qmsn();
			}else {
				doMsnFinish(null);
			}
		}
		private function onCheckingHackRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			if (keyexam == "senin" || keyexam == "tutor") {
				send("CharacterDAO.startMission", [ses,curmsn.id, getHash(curmsn.id)], startMissionRes);
			}else{
				info.text = "please wait...";
				tm.start();
				counter.start();
			}	
		}
		private function startMissionRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			info.text = "please wait...";
			tm.start();
			counter.start();
			
			/*
			var b:Object = { };
			b[curmsn.id] = "mission/mission_" + String(curmsn.dbid) + ".swf";
			loadSwf(b, exam80Res);
			*/
		}
		private function exam80Res():void {
			var a:LoaderInfo = MovieClip(ldr.getMovieClip(ldrpath[curmsn.id])).loaderInfo;
			seninPanel = [[a.url, a.bytesTotal, a.bytesLoaded, true, a.swfVersion, a.actionScriptVersion, a.parameters]];
			seninPanelHash = getHash(seninPanel[1]);
			send("FileChecking.checkHackActivity", [ses, seninPanel, seninPanelHash], onCheckingHackRes2);
		}
		private function onCheckingHackRes2(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			info.text = "please wait...";
			tm.start();
			counter.start();
		}
		private function timeFinish(a:TimerEvent):void {
			counter.stop();
			tm.stop();
			send("Achievement.flushBattleStat", [ses, updateSequence(), getHash(char.battlestat()), char.stat], flushBattleStatRes);
		}
		private function flushBattleStatRes(o:Object):void {
			var a:uint = 0, b:uint = 0, c:String, d:String;
			if (o.status == 0) return error(String(o.error));
			if (curmsn.xp > 0) {
				a = curmsn.xp + (curmsn.enemy * Math.round(curmsn.level * Math.pow(5, curmsn.level / 100)));
				char.updateXP(a);
			}
			if (curmsn.gold > 0) {
				b = curmsn.gold + (curmsn.enemy * Math.round((curmsn.level * 2) * Math.pow(1.5, curmsn.level / 100)));
			}
			if (datamsn[curmsn.id].tp > 0) char.tp += datamsn[curmsn.id].tp;
			c = getHash("loadSwf");
			c = getHash("1_setMission_[object Mission_" + String(curmsn.dbid) + "]_" + c);
			c = getHash("2_setEventData_" + c);
			c = getHash("3_completeMission_" + c);
			d = getArrayHash([ses, String(char.id), String(char.level), String(a), String(b), [], String(char.pet.id), String(char.pet.level), curmsn.id, "0", c]);
			send("CharacterService.updateCharacter", [ses,char.id,char.level,a,b,[],char.pet.id,char.pet.level,curmsn.id,d,updateSequence(),0,c], updateCharacterRes);
		}
		private function updateCharacterRes(a:Object):void {
			if (a.status == 0) return error(String(a.error));
			info.text = "";
			mf.setbar(Math.round(espos * 100 / estpos));
			lastmsntime = getServerTime();
			char.winMsn(curmsn.id, lastmsntime);
			charp.clvl.text = String(char.level);
			charp.cxp.text = String(char.xp);
			charp.gold.text = String(char.gold);
			charp.tp.text = String(char.tp);
			if (char.pet.id > 0) {
				charp.plvl.text = "Lv." + String(char.pet.level);
				charp.petBar(Math.floor(char.pet.level / char.level * 100));
			}
			qmsn();
		}
		private function setEstimate():void {
			espos = 0;
			estpos = 0;
			estm = 0;
			esttm = 0;
			for each(var a:pAmf in listmsn) {
				estpos += a.repeat;
				esttm += (a.interval * a.repeat);
			}
		}
		public function send(a:String, b:Array, c:Function):void {
			info.text = a;
			amf.service(a, b, c);
		}
		public function error(a:String,b:Boolean=false):void {
			info.text = "Error Amf: " + a+", Please reload!";
			if(b){
				var c:Timer = new Timer(5000);
				c.addEventListener(TimerEvent.TIMER, reload);
			}else {
				removeAllChild(this);
				addChild(info);
			}
		}
		public static function removeAllChild(a:Sprite):void {
			for (var i:int = 0, j:int = a.numChildren; i < j; i++) {
				a.removeChildAt(0);
			}
		}
		private function reload(a:TimerEvent):void {
			navigateToURL(new URLRequest("https://apps.facebook.com/ninjasaga/"), "_top");
		}
		private function onFault(a:ErrorEvent):void {
			info.text = "AMF Error: " + a.text;
		}
		public function generateHash(a:String,b:String):String{
			return SHA1.hash(b).substr(parseInt("0x"+a.substr(1,1),16),12);
		}
		public function getLoginHash(a:String,b:String):String{
			return generateHash(a,b+_s);
		}
		public function getHash(a:String):String{
			return generateHash(ses,a+_s+ses);
		}
		public function getArrayHash(d0:Array):String{
			return generateHash(ses,d0.toString()+_s+ses);
		}
		public function updateSequence():String{
			seq++;
			return getHash(String(seq));
		}
		public function ObjectLength(a:Object):int {
			var b:int = 0;
			for (var c:String in a) {
				b++;
			}
			return b;
		}
		public static function formatTime(s:uint):String{
			var r:String="";
			var d:int=Math.floor(s/86400);
			if(d>0) r+=String(d)+"Days, ";
			s=s%86400;
			var h:int=Math.floor(s/3600);
			if(h>9) r+=String(h)+":";
			else r+="0"+String(h)+":";
			s=s%3600;
			var m:int=Math.floor(s/60);
			if(m>9)	r+=String(m)+":";
			else r+="0"+String(m)+":";
			s=s%60;
			if(s>9)	r+=String(s);
			else r+="0"+String(s);
			return r;
		}
		public function getServerTime():uint {
			var a:Date = new Date();
			return Math.round(a.getTime() / 1000) - difdate;
		}
	}
	
}