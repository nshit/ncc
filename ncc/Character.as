package ncc {
	public class Character {
		public var id:uint;
		public var name:String;
		public var level:uint;
		public var xp:uint;
		public var rank:uint;
		public var gold:uint;
		public var tp:uint;
		public var control:uint;
		public var gender:String;
		public var msn:Object;
		public var stat:Object = { "1":1, "2":0, "3":1, "4":0, "5":0, "7":1, "8":0, "9":0, "10":0, "11":0 };
		public var pet:Pet;
		public var startSJ:int = 0;
		public var startTutor:int = 0;
		public var noticeTutor:int = 0;
		public var tutorLeft:int = -1;
		public var tutorConfirm:Boolean = true;
		public var tutorHard:Boolean = true;
		
		public function Character(a:Object):void {
			id = int(a.character_id);
			name = a.character_name;
			level = int(a.character_level);
			xp = int(a.character_xp);
			rank = int(a.character_rank);
			gold = int(a.character_gold);
			tp = int(a.character_bloodline);
			control = int(a.character_control);
			gender = a.character_gender;
			parseMsn(a.character_mission.split(","));
			pet = new Pet();
		}
		public function battlestat():String {
			var a:Array = [];
			for (var b:String in stat) {
				a.push(String(stat[b]));
			}
			return "Achievement.flushBattleStat"+a.join(",");
		}
		public function parsePet(a:Array):void {
			pet = new Pet;
			for (var b:int = 0, c:int = a.length; b < c;b++) {
				if (a[b].equipped == true) {
					pet.id = a[b].id;
					pet.name = a[b].name;
					pet.level = a[b].level;
					pet.xp = a[b].xp;
					break;
				}
			}
		}
		private function parseMsn(a:Array):void {
			msn = { };
			if (a.length == 0) return;
			var b:Array;
			for (var i:int = 0, j:int = a.length; i < j; i++) {
				b = String(a[i]).split(":");
				msn["msn"+b[0]] = [int(b[1]), int(b[2]), int(b[3])];
			}
		}
		public function getMsn(a:String):Array {
			if (msn[a]) return msn[a];
			return [0, 0, 0];
		}
		public function winMsn(a:String,b:uint):void {
			if (msn[a]) {
				msn[a][0]++;
				msn[a][2] = b;
			}else msn[a] = [1,0,b];
		}
		public function updateGold(a:int):void {
			gold += a;
		}
		public function updateXP(a:int):void {
			if (pet.id > 0) {
				if (pet.level < level) pet.updateXP(Math.round(a*0.2));
			}
			if (level == 20 && rank < 2) return;
			if (level == 40 && rank < 4) return;
			if (level == 60 && rank < 6) return;
			if (level == 80) return;
			xp += a;
			level = getLvByXp(xp);
		}
        public static function getLvByXp(a:uint):uint{
            var b:uint = 130;
            var c:uint = 50;
            var d:uint = 50;
            var e:uint;
            var f:uint;
            while (e <= a) {
                f++;
                e = (e + Math.round(((f * b) * Math.pow(c, (f / d)))));
            };
            return (f);
        }
        public static function getXpByLv(a:uint):uint{
            var b:uint = 130;
            var c:uint = 50;
            var d:uint = 50;
            var e:uint;
            var f:uint = 1;
            while (f < a) {
                e = (e + Math.round(((f * b) * Math.pow(c, (f / d)))));
                f++;
            };
            if (e < 0){
                e = 0;
            };
            return (e);
        }
	}
}