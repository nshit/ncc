package ncc {
	public class Pet {
		public var id:uint=0;
		public var name:String="";
		public var level:uint=1;
		public var xp:uint = 0;
		
		public function Pet():void {
			
		}
		public function updateXP(a:int):void {
			xp += a;
			level = getPetLvByXp(xp);
		}
        public static function getPetXpByLv(a:uint):uint{
            var b:uint = 130;
            var c:uint = 50;
            var d:uint = 50;
            var e:uint;
            var f:uint = 1;
            while (f < a) {
                e = (e + Math.round((((f * b) * Math.pow(c, (f / d))) * 0.2)));
                f++;
            };
            if (e < 0){
                e = 0;
            };
            return (e);
        }
        public static function getPetLvByXp(a:uint):uint{
            var b:uint = 130;
            var c:uint = 50;
            var d:uint = 50;
            var e:uint;
            var f:uint;
            while (e <= a) {
                f++;
                e = (e + Math.round((((f * b) * Math.pow(c, (f / d))) * 0.2)));
            };
            return (f);
        }
		
	}
}