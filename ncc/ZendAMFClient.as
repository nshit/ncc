package ncc {
    import flash.events.EventDispatcher;
    import flash.net.NetConnection;
    import flash.utils.Timer;
    import flash.events.NetStatusEvent;
    import flash.events.SecurityErrorEvent;
    import flash.events.IOErrorEvent;
    import flash.events.ErrorEvent;
    import flash.net.Responder;
    import flash.events.TimerEvent;
    import flash.events.Event;

    public class ZendAMFClient extends EventDispatcher {

        private static var instance:ZendAMFClient;
        private static var gateway:String;
        private static var connection:NetConnection;
        private static var serviceArr:Array = [];
        private static var curService:Object = null;
        private static var onService:Boolean = false;

        public function ZendAMFClient(_arg_1:SingletonBlocker):void{
            super();
            if (_arg_1 == null){
                throw (new Error("Error: Instantiation failed: Use ZendAMFClient.getInstance() instead of new."));
            };
            gateway = "";
            connection = new NetConnection();
        }
        public static function getInstance():ZendAMFClient{
            if (instance == null){
                instance = new (ZendAMFClient)(new SingletonBlocker());
            };
            return (instance);
        }

        public function connect(_gateway:String):void{
            gateway = _gateway;
            try {
                connection.connect(gateway);
                connection.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
                connection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
                connection.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            }
            catch(err:Error) {
                dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ("Error:" + err.message)));
            };
        }
		public function disconnect():void{
			gateway="";
            try {
                connection.removeEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
                connection.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
                connection.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
            }
            catch(err:Error) {
                dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ("Error:" + err.message)));
            };
		}
        public function service(_arg_1:String, _arg_2:Array, _arg_3:Function):void{
            serviceArr.push({
                "serviceName":_arg_1,
                "args":_arg_2,
                "callBackFn":_arg_3
            });
            if (!onService){
                process();
            };
        }
        private function localCallBack(_arg_1:Object):void{
            curService.callBackFn(_arg_1);
            process();
        }
        private function process():void{
            var _serviceName:String;
            var _args:Array;
            var responder:Responder;
            var args:Array;
            if (gateway == ""){
                dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR, false, false, "Connection not available"));
            }
            else {
                if (serviceArr.length > 0){
                    onService = true;
                    curService = serviceArr.shift();
                    _serviceName = curService.serviceName;
                    _args = curService.args;
                    responder = new Responder(localCallBack, onFault);
                    args = new Array(_serviceName, responder);
                    try {
                        connection.call.apply(null, args.concat(_args));
                    }
                    catch(err:Error) {
                        dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ("Error:" + err.message)));
                    };
                }
                else {
                    onService = false;
                };
            };
        }
        public function isConnected():Boolean{
            return ((((gateway)=="") ? false : true));
        }
        private function onFault(_arg_1:Object):void{
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, _arg_1.description));
            process();
        }
        override public function dispatchEvent(_arg_1:Event):Boolean{
            if (((hasEventListener(_arg_1.type)) || (_arg_1.bubbles))){
                return (super.dispatchEvent(_arg_1));
            };
            return (true);
        }
        public function netStatusHandler(_arg_1:NetStatusEvent):void{
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ("NetStatus Error:" + _arg_1.info.code)));
            process();
        }
        public function securityErrorHandler(_arg_1:SecurityErrorEvent):void{
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ("Security Error:" + _arg_1.text)));
            process();
        }
        public function ioErrorHandler(_arg_1:IOErrorEvent):void{
            dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ("IO Error:" + _arg_1.text)));
            process();
        }

    }
}

class SingletonBlocker {

}

