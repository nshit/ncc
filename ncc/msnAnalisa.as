package ncc {
	public class msnAnalisa {
		public var data:pAnalisa;
		private var trash:Array;
		
		public function msnAnalisa():void {
			data = new pAnalisa();
		}
		public function xp(a:uint,b:uint,t:uint=10):String {
			var c:uint = Character.getLvByXp(a), d:uint = c,e:pAmf,f:Boolean;
			generateMsn();
			data.clear();
			trash = [];
			e = byxp(c);
			while(a<b&&data.length<500){
				if (e == null) break;
				e.interval = t;
				data.add(e);
				f = e.special;
				a += e.xp;
				c = Character.getLvByXp(a);
				if (c > d) {
					d = c;
					f = true;
				}
				if (f) e = byxp(c);
			}
			return data.toString(c);
		}
		public function gold(a:uint, b:uint, c:uint,t:uint=10):String {
			var d:uint = Character.getLvByXp(a), e:uint = d, f:pAmf,g:uint=0,h:Boolean;
			generateMsn();
			data.clear();
			trash = [];
			f = bygold(d);
			while (g < c && data.length < 500) {
				if (f == null) break;
				f.interval = t;
				data.add(f);
				h = f.special;
				g += f.gold;
				if (a < b) {
					a += f.xp;
					d = Character.getLvByXp(a);
					if (d > e) {
						e = d;
						h = true;
					}
				}
				if (h) f = bygold(d);
			}
			return data.toString(d);
		}
		private function byxp(a:uint):pAmf {
			var b:Array = highLevel(a);
			b.sortOn(["xp","gold"], Array.DESCENDING | Array.NUMERIC);
			if (b.length > 0) return b.shift();
			return null;
		}
		private function bygold(a:uint):pAmf {
			var b:Array = highLevel(a);
			b.sortOn(["gold","xp"], Array.DESCENDING | Array.NUMERIC);
			if (b.length > 0) return b.shift();
			return null;
		}
		private function highLevel(a:uint):Array {
			var c:Array = [];
			for each(var b:pAmf in base.msnana) {
				if (b.level > a) continue;
				if (trash.indexOf(b.id) >= 0) continue;
				c.push(b);
				if (base.main.datamsn[b.id].special) trash.push(b.id);
			}
			return c;
		}
		private function generateMsn():void {
			var b:*,c:Boolean=base.main.acc.prem!="2"?true:false,d:int=base.main.char.level,e:Array,f:Boolean,g:uint=base.main.curdate,h:pAmf;
			if (base.msnana==null) {
				base.msnana = new Vector.<pAmf>();
				for each(b in base.main.datamsn) {
					if (base.unlist.indexOf(b.dbid) >= 0) continue;
					if (c && b.premium == true) continue;
					if (b.vendor == false) continue;
					e = base.main.char.getMsn(b.id);
					f = true;
					if (b.special) {
						if (e[0] > 0) {
							if (b.daily && g > 0) {
								if (e[2] >= g) f = false;
							}else f = false;
						}
						if (b.tp > 0) f = false;
					}
					if (f) {
						h = new pAmf(b.id, b.level, b.xp, b.gold,0,1,10,b.special);
						if (base.data[h.id]) {
							if (base.data[h.id].xp && base.data[h.id].xp > b.xp) h.xp = base.data[h.id].xp;
							if (base.data[h.id].gold && base.data[h.id].gold > h.gold) h.gold = base.data[h.id].gold;
						}
						base.msnana.push(h);
					}
				}
			}
		}
	}
}