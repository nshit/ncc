package ncc {
	public class pAmf {
		public var dbid:uint;
		public var id:String;
		public var level:uint;
		public var xp:uint;
		public var gold:uint;
		public var enemy:uint;
		public var repeat:uint;
		public var interval:uint;
		public var special:Boolean;
		public function pAmf(a:String, b:uint, c:uint = 0, d:uint = 0, e:uint = 0, f:uint = 1, g:uint = 10,h:Boolean=false):void {
			dbid = int(a.substr(3));
			id = a;
			level = b;
			xp = c;
			gold = d;
			enemy = e;
			repeat = f;
			interval = g;
			special = h;
		}
	}
}