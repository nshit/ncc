package ncc {
	public class pAnalisa {
		public var time:uint=0;
		public var process:uint=0;
		public var xp:uint=0;
		public var gold:uint=0;
		public var data:Vector.<pAmf>;
		public var length:uint = 0;
		private var lastid:String = "";
		
		public function pAnalisa():void {
			data = new Vector.<pAmf>();
		}
		public function add(a:pAmf):void {
			if (a.id != lastid||length==0) {
				data.push(a);
				length++;
				lastid = a.id;
			}else {
				data[length - 1].repeat += 1;
			}
			time+= a.interval;
			process++;
			xp += a.xp;
			gold += a.gold;
		}
		public function toString(a:uint):String {
			return "Total Process " + String(process) + ", Estimated Time " + formatTime(time) + ", Up to Level " + String(a)+", XP: "+String(xp)+", Gold: "+String(gold);
		}
		public function clear():void {
			lastid = "";
			data.length = 0;
			length = 0;
			time = 0;
			process = 0;
			xp = 0;
			gold = 0;
		}
		public function formatTime(s:uint):String{
			var r:String="";
			var d:int=Math.floor(s/86400);
			if(d>0) r+=String(d)+"Days, ";
			s=s%86400;
			var h:int=Math.floor(s/3600);
			if(h>9) r+=String(h)+":";
			else r+="0"+String(h)+":";
			s=s%3600;
			var m:int=Math.floor(s/60);
			if(m>9)	r+=String(m)+":";
			else r+="0"+String(m)+":";
			s=s%60;
			if(s>9)	r+=String(s);
			else r+="0"+String(s);
			return r;
		}
		
	}
}